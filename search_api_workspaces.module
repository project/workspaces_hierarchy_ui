<?php

/**
 * @file
 * Contains search_api_fulltext_translations.module.
 */

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Item\Field;
use Drupal\search_api\Plugin\search_api\datasource\ContentEntity;
use Drupal\search_api_workspaces\Plugin\search_api\datasource\WorkspacesContentEntity;

/**
 * Implements hook_help().
 */
function search_api_workspaces_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the search_api_fulltext_translations module.
    case 'help.page.search_api_workspaces':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Adds custom fields for every existing fulltext field, with all translations included.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_entity_insert().
 *
 * Adds entries for all languages of the new entity to the tracking table for
 * each index that tracks entities of this type.
 *
 * By setting the $entity->search_api_skip_tracking property to a true-like
 * value before this hook is invoked, you can prevent this behavior and make the
 * Search API ignore this new entity.
 *
 * Note that this function implements tracking only on behalf of the "Content
 * Entity" datasource defined in this module, not for entity-based datasources
 * in general. Datasources defined by other modules still have to implement
 * their own mechanism for tracking new/updated/deleted entities.
 *
 * @see \Drupal\search_api\Plugin\search_api\datasource\ContentEntity
 */
function search_api_workspaces_entity_insert(EntityInterface $entity) {
  // Check if the entity is a content entity.
  if (!($entity instanceof ContentEntityInterface) || $entity->search_api_skip_tracking) {
    return;
  }
  $indexes = ContentEntity::getIndexesForEntity($entity);
  if (!$indexes) {
    return;
  }

  // Compute the item IDs for all languages of the entity.
  $item_ids = [];
  $entity_id = $entity->id();
  $revision_id = $entity->get('vid')->value;
  foreach (array_keys($entity->getTranslationLanguages()) as $langcode) {
    $item_ids[] = $entity_id . ':' . $langcode . ':' . $revision_id;
  }
  $datasource_id = 'workspaces_entity:' . $entity->getEntityTypeId();
  foreach ($indexes as $index) {
    $filtered_item_ids = ContentEntity::filterValidItemIds($index, $datasource_id, $item_ids);
    $index->trackItemsInserted($datasource_id, $filtered_item_ids);
  }
}

/**
 * Implements hook_entity_update().
 *
 * Updates the corresponding tracking table entries for each index that tracks
 * this entity.
 *
 * Also takes care of new or deleted translations.
 *
 * By setting the $entity->search_api_skip_tracking property to a true-like
 * value before this hook is invoked, you can prevent this behavior and make the
 * Search API ignore this update.
 *
 * Note that this function implements tracking only on behalf of the "Content
 * Entity" datasource defined in this module, not for entity-based datasources
 * in general. Datasources defined by other modules still have to implement
 * their own mechanism for tracking new/updated/deleted entities.
 *
 * @see \Drupal\search_api\Plugin\search_api\datasource\ContentEntity
 */
function search_api_workspaces_entity_update(EntityInterface $entity) {
  // Check if the entity is a content entity.

  if (!($entity instanceof ContentEntityInterface) || $entity->search_api_skip_tracking) {
    return;
  }
  $indexes = WorkspacesContentEntity::getIndexesForEntity($entity);

  if (!$indexes) {
    return;
  }

  // Compare old and new languages for the entity to identify inserted,
  // updated and deleted translations (and, therefore, search items).

  // we enforce creation of revisions always delete revision so no update
  $entity_id = $entity->id();
  $old_revision_id = $entity->original->getRevisionId();
  $revision_id = $entity->getRevisionId();
  // Build item ids based on translations
  $insert_item_ids = array_keys($entity->getTranslationLanguages());
  $deleted_item_ids = [];
  $old_translations = $entity->original->getTranslationLanguages();

  foreach ($old_translations as $langcode => $language) {
    $deleted_item_ids[] = $langcode;
  }

  // soft delete
  if($entity->get('deleted')->value)  {
    foreach ($old_translations as $langcode => $language) {
      $deleted_item_ids[] = $langcode;
    }
  }

  $datasource_id = 'workspaces_entity:' . $entity->getEntityTypeId();

  $combine_id_old = function ($langcode) use ($entity_id, $old_revision_id) {
    return $entity_id . ':' . $langcode . ':' . $old_revision_id;
  };
  $combine_id_new = function ($langcode) use ($entity_id, $revision_id) {
    return $entity_id . ':' . $langcode . ':' . $revision_id;
  };

  $insert_item_ids = array_map($combine_id_new, $insert_item_ids);
  $deleted_item_ids = array_map($combine_id_old, $deleted_item_ids);

  foreach ($indexes as $index) {
    if ($insert_item_ids) {
      $filtered_item_ids = WorkspacesContentEntity::filterValidItemIds($index, $datasource_id, $insert_item_ids);
      $index->trackItemsInserted($datasource_id, $filtered_item_ids);
    }
    if ($deleted_item_ids) {
      $index->trackItemsDeleted($datasource_id, $deleted_item_ids);
    }
  }

}

/**
 * Implements hook_entity_delete().
 *
 * Deletes all entries for this entity from the tracking table for each index
 * that tracks this entity type.
 *
 * By setting the $entity->search_api_skip_tracking property to a true-like
 * value before this hook is invoked, you can prevent this behavior and make the
 * Search API ignore this deletion. (Note that this might lead to stale data in
 * the tracking table or on the server, since the item will not removed from
 * there (if it has been added before).)
 *
 * Note that this function implements tracking only on behalf of the "Content
 * Entity" datasource defined in this module, not for entity-based datasources
 * in general. Datasources defined by other modules still have to implement
 * their own mechanism for tracking new/updated/deleted entities.
 *
 * @see \Drupal\search_api\Plugin\search_api\datasource\WorkspacesContentEntity
 */
function search_api_workspaces_entity_delete(EntityInterface $entity) {

  // Check if the entity is a content entity.
  if (!($entity instanceof ContentEntityInterface) || $entity->search_api_skip_tracking) {
    return;
  }
  $indexes = WorkspacesContentEntity::getIndexesForEntity($entity);
  if (!$indexes) {
    return;
  }

  // Remove the search items for all the entity's translations.
  $item_ids = [];
  $entity_id = $entity->id();
  $revision_id = $entity->getRevisionId();
  foreach (array_keys($entity->getTranslationLanguages()) as $langcode) {
    $item_ids[] = $entity_id . ':' . $langcode . ':' . $revision_id;
  }
  $datasource_id = 'workspaces_entity:' . $entity->getEntityTypeId();

  foreach ($indexes as $index) {
    $index->trackItemsDeleted($datasource_id, $item_ids);
  }
}
